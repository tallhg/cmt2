﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CMT2.Models;

namespace CMT2.Models
{
    public class CMT2Context : DbContext
    {
        public CMT2Context (DbContextOptions<CMT2Context> options)
            : base(options)
        {
        }

        public DbSet<CMT2.Models.User> User { get; set; }

        public DbSet<CMT2.Models.Race> Race { get; set; }

        public DbSet<CMT2.Models.RaceTime> RaceTime { get; set; }
    }
}
