﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMT2.Models
{
    public class LeaderBoard
    {
        public RaceTime[] RaceTimes {get;set;}
        public bool IsMore { get; set; }
    }
}
