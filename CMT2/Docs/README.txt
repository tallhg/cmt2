﻿

Hi, hope this all works for you, it's a bit templaty I'm afraid, but time is short this week.
I have not inclued foreign keys in the sql, as I have been using EF for relationships at the moment.
there's no authentication etc either.

usually i would have view models and a service layer with a repository 
and unitofwork to abstract the DB away.



So, a very simple schema here to save racetimes, users and races. using int for everything including the time

TABLES

create table RaceTime (
RaceTimeId [int] identity(1,1) primary key,
UserId [int] not null,
RaceId [int] not null,
Time [int] not null)

create table Race (
RaceId [int] identity(1,1) primary key,
Name [varchar](50) not null)

create table dbo.Users (
UserId [int] identity(1,1) primary key,
UserName [varchar](50) not null)


REST API as follows 
1. create a user 
POST : http://localhost:51344/api/users
body : {username :"rob"}

2. search for user by name
http://localhost:51344/api/users/query?username=rob

3. get user by id
http://localhost:51344/api/users/1


4. create a race
POST :http://localhost:51344/api/races
body : {racename :"silverstone"}

5. search for race by name
http://localhost:51344/api/races/query?racename=silverstone

6. add a race time (race,user,time)
http://localhost:51344/api/racetimes
body : {userid : 3, raceid : 1, time : 5300}

7. get leaderboard for race id, filter by page
a http://localhost:51344/api/RaceTimes/LeaderBoard?raceid=1
b http://localhost:51344/api/RaceTimes/LeaderBoard?raceid=1&page=1

these return a simple 'leaderboard' model including a list of 'PAGESIZE' racetimes
, and a bool to say if there is more data to come ; include page=x on the query to 
get each page. This could be better by proving links to access next and previous pages etc.
also, where i return the user or race, it would perhaps be better to return a link to access
them if required by the client.

Regards,
Rob.