﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CMT2.Models;

namespace CMT2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RaceTimesController : ControllerBase
    {
        private readonly CMT2Context _context;
        private static int PAGESIZE = 5;

        public RaceTimesController(CMT2Context context)
        {
            _context = context;
        }

        // GET: api/RaceTimes
        [HttpGet]
        public IEnumerable<RaceTime> GetRaceTime()
        {
            return _context.RaceTime;
        }

        // GET: api/RaceTimes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRaceTime([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var raceTime = await _context.RaceTime.FindAsync(id);

            // IF WE LIKE TO LOAD RELATIONSHIPS :
            _context.Entry(raceTime).Reference(r => r.Race).Load();
            _context.Entry(raceTime).Reference(r => r.User).Load();



            if (raceTime == null)
            {
                return NotFound();
            }

            return Ok(raceTime);
        }

        // GET: api/RaceTimes/query?raceid=1
        [HttpGet("LeaderBoard")]
        public async Task<IActionResult> GetLeaderBoard([FromQuery] int raceid, [FromQuery] int page = 0)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // - TODO validate input before using it !!

            var query = $"select * from [RaceTime] where RaceId = '{raceid}'";
            var raceTimesQuery = _context.RaceTime.FromSql(query).OrderBy(r => r.Time).Include(r => r.User);
            var totalRaceTimes = raceTimesQuery.Count();
            if (raceTimesQuery.Count() == 0)
            {
                return NotFound();
            }
            var totalPages = (int)Math.Ceiling(raceTimesQuery.Count() / (double)PAGESIZE);
            if (page >= totalPages )
            {
                return NotFound();
            }

            var racetimes = await raceTimesQuery.Skip(page  * PAGESIZE).Take(PAGESIZE).ToListAsync();

            LeaderBoard leaderBoard = new LeaderBoard();
            leaderBoard.RaceTimes = racetimes.ToArray();
            leaderBoard.IsMore = (page < (totalPages -1 ));

            return Ok(leaderBoard);
        }

        // PUT: api/RaceTimes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRaceTime([FromRoute] int id, [FromBody] RaceTime raceTime)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != raceTime.RaceTimeId)
            {
                return BadRequest();
            }

            _context.Entry(raceTime).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RaceTimeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RaceTimes
        [HttpPost]
        public async Task<IActionResult> PostRaceTime([FromBody] RaceTime raceTime)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.RaceTime.Add(raceTime);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRaceTime", new { id = raceTime.RaceTimeId }, raceTime);
        }

        // DELETE: api/RaceTimes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRaceTime([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var raceTime = await _context.RaceTime.FindAsync(id);
            if (raceTime == null)
            {
                return NotFound();
            }

            _context.RaceTime.Remove(raceTime);
            await _context.SaveChangesAsync();

            return Ok(raceTime);
        }

        private bool RaceTimeExists(int id)
        {
            return _context.RaceTime.Any(e => e.RaceTimeId == id);
        }
    }
}