﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CMT2.Models;

namespace CMT2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RacesController : ControllerBase
    {
        private readonly CMT2Context _context;

        public RacesController(CMT2Context context)
        {
            _context = context;
        }

        // GET: api/Races
        [HttpGet]
        public IEnumerable<Race> GetRace()
        {
            return _context.Race;
        }

        // GET: api/Races/query?racename=
        [HttpGet("query")]
        public async Task<IActionResult> GetRaceByQuery([FromQuery] string racename)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // - TODO validate input before using it !!
            if (string.IsNullOrEmpty(racename))
            {
                return BadRequest(ModelState);
            }
            var query = $"select * from [Race] where raceName = '{racename}'";
            var race = await _context.Race.FromSql(query).FirstOrDefaultAsync();

            if (race == null)
            {
                return NotFound();
            }

            return Ok(race);
        }

        // GET: api/Races/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRace([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var race = await _context.Race.FindAsync(id);

            if (race == null)
            {
                return NotFound();
            }

            return Ok(race);
        }

        // PUT: api/Races/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRace([FromRoute] int id, [FromBody] Race race)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != race.RaceId)
            {
                return BadRequest();
            }

            _context.Entry(race).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RaceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Races
        [HttpPost]
        public async Task<IActionResult> PostRace([FromBody] Race race)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Race.Add(race);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRace", new { id = race.RaceId }, race);
        }

        // DELETE: api/Races/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRace([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var race = await _context.Race.FindAsync(id);
            if (race == null)
            {
                return NotFound();
            }

            _context.Race.Remove(race);
            await _context.SaveChangesAsync();

            return Ok(race);
        }

        private bool RaceExists(int id)
        {
            return _context.Race.Any(e => e.RaceId == id);
        }
    }
}